import { useState } from 'react'
import './App.css'

import Dat from "../src/components/Dat"
import Inf from "../src/components/Inf"

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
    <body>
      
      <div className="d">
        <Dat />
      </div>
      
      <div className="i">
        <Inf />
      </div>

    </body>
    </>
  )
}

export default App
